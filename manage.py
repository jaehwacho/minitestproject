#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jaehwacho'
from flask.ext.script import Manager
from minisat.main import application
from minisat.model import init_db, clear_db, calcurate_all_error_rate

manager = Manager(application)

#실행방법
#python manage.py intdb
@manager.command
def initdb():
    """Initialize database."""
    with application.app_context():
        init_db()


#실행방법
#python manage.py cleardb
@manager.command
def cleardb():
    """Clear database."""
    with application.app_context():
        clear_db()

#실행방법
#python manage.py calcurateErrorRate 2013-11-15
@manager.command
def calcurateErrorRate(input_date):
    """Calculater EaxmResult"""
    print "input:", input_date
    with application.app_context():
        calcurate_all_error_rate(input_date)


if __name__ == '__main__':
    manager.run()

