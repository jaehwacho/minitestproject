#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jaehwacho'
from minisat.main import application

if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)
