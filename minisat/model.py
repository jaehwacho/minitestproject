#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jaehwacho'

from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base
import pickle
import random
import datetime
import json

db_engine = None
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False))

def init_engine(db_uri):
    global db_engine
    db_engine = create_engine(db_uri, convert_unicode=True)
    db_session.configure(bind=db_engine)


def init_db():
    Base.metadata.create_all(bind=db_engine)

    #수학(2014수능A - 홀수형)
    answers = [1,3,5,2,4,1,2,3,4,5,3,2,4,1,5,1,3,5,5,2,4]
    points =  [2,2,2,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4]
    image_url_format = u"https://s3-ap-northeast-1.amazonaws.com/suneung-test-service/test/2014suneung/mathA/%d.jpg"
    name_format = u"2014수능 수학A 홀수형 %d번"
    subjectName = u'math'
    questions_math = _make_sample_question(answers, points, subjectName, image_url_format, name_format)

    #수학(2014수능B - 홀수형)
    answers = [4,3,5,1,2,1,3,4,4,5,1,2,3,4,3,2,5,4,2,5,1]
    points =  [2,2,2,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4]
    image_url_format = u"https://s3-ap-northeast-1.amazonaws.com/suneung-test-service/test/2014suneung/mathB/%d.jpg"
    name_format = u"2014수능 수학B 홀수형 %d번"
    subjectName = u'math'
    questions = _make_sample_question(answers, points, subjectName, image_url_format, name_format)
    questions_math.extend(questions)

    db_session.add_all(questions_math)

    #영어(2014수능A - 홀수형)
    answers = [4,2,4,1,3,1,3,5,5,1,2,2,5,5,2,4,4,1,3,1,3,2,5,3,5,2,4,3,5,3,5,4,3,1,1,2,2,4,4,2,1,2,4,3,5]
    points =  [2,2,3,2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,3,2,2,2,2,2,2,2,3,3,2,2,2,2,3,3,3,2,2,2,3,2,2,3,2,2,2]
    image_url_format = u"https://s3-ap-northeast-1.amazonaws.com/suneung-test-service/test/2014suneung/english/suneung_2014_english_b_%d.jpg"
    name_format = u"2014수능 영어B 홀수형 %d번"
    subjectName = u'english'
    questions_english = _make_sample_question(answers, points, subjectName, image_url_format, name_format)
    db_session.add_all(questions_english)

    #11/13~(20days) 테스트 데이터를 랜덤으로 선택하여 입력함)
    #문제 선별-수학
    #전체 문제중 랜덤선택
    _make_daily_test(questions_math,
                     range(len(questions_math)),
                     'math',
                     datetime.date(2013,11,13), 20)

    ##문제 선별-영어
    ##22~40 중 랜덤선택
    _make_daily_test(questions_english,
                     range(22, 41),
                     'english',
                     datetime.date(2013,11,13), 20)


    #Config 설정
    app_config = {}
    app_config['active_user_count'] = 100
    app_config['target_user_count'] = 1000
    app_config['market_url'] = 10
    app_config['apk_version'] = 10
    app_config['market_url'] = "market://details?id=com.dulgi.MoGye&utm_medium=referral&utm_source=update&utm_campaign=33"
    app_config['apk_version'] = 10
    app_config['apk_upgrade_force'] = True
    app_config['apk_upgrade_message'] = "수능 문제 채점이 가능합니다!\n최신 버전으로 업데이트 뒤 채점하세요!"
    app_config['question_deadline'] = "00:00"
    for key, value in app_config.iteritems():
        item = Config(name=key, value=pickle.dumps(value), active=True)
        db_session.add(item)
    db_session.commit()

def clear_db():
    Base.metadata.drop_all(bind=db_engine)

#시험오답률 계산한기
def calcurate_error_rate(input_date, subject):
    print "calculate_exam_data subject:", subject
    exam_date = datetime.datetime.strptime(input_date , '%Y-%m-%d').date()
    print "exam_date:", exam_date
    data = UserAnswer.query.filter(UserAnswer.subject==subject, UserAnswer.date==exam_date)

    user_info = {}
    for item in data:
        answers = json.loads(item.answers)
        for answer in answers:
            id = answer.get("id")

            if user_info.has_key(id) is False:
                user_info[id] = {}
                user_info[id]['correct_count'] = 0
                user_info[id]['incorrect_count'] = 0

            if answer.get("correct_answer") is True:
                user_info[id]['correct_count'] += 1
            else:
                user_info[id]['incorrect_count'] += 1

    print "user_info:", user_info
    for answer_id, answer_data in user_info.iteritems():
        correct_count  = answer_data.get('correct_count',0)
        incorrect_count = answer_data.get('incorrect_count',0)
        questionInfo = QuestionInfo.query.get(answer_id)
        questionInfo.correct_count = correct_count
        questionInfo.incorrect_count = incorrect_count
        if incorrect_count != 0:
            questionInfo.error_rate = incorrect_count*1.0 / (correct_count+incorrect_count)*1.0
        else:
            questionInfo.error_rate = 0

    db_session.commit()

#전과목(수학, 영어) 시험오답률 계산한기
def calcurate_all_error_rate(input_date):
    subjects = ["math", "english"]
    for subject in subjects:
        calcurate_error_rate(input_date, subject)

#문제 만들기
def _make_sample_question(answers, points, subjectName, image_url_format, name_format):
    questions = []
    images = []
    questionNames = []
    size = len(answers)
    for index in range(size):
        url = image_url_format%(index+1)
        images.append(url)
        questionNames.append(name_format%(index+1))

    for index in range(size):
        question_image_url = images[index]
        question = Question(subject=subjectName,
                            question_image_url=question_image_url,
                            point=points[index-1],
                            answer=str(answers[index]),
                            name=questionNames[index])
        questions.append(question)
    return questions

#Daily Test 만들기 (과목별 5개 문제 랜덤선택)
def _make_daily_test(questions, questionIds, subject, startDay, days):
    for day in range(days):
        nextDay = startDay + datetime.timedelta(days=day)
        selectedQuestions = random.sample(set(questionIds), 5)
        questionInfos = []
        for id in selectedQuestions:
            questionInfo = QuestionInfo(question=questions[id])
            questionInfos.append(questionInfo)
        test = Test(subject=subject, time=nextDay)
        test.questioninfos = set(questionInfos)

class Base(object):
    id = Column(Integer, primary_key=True)

Base = declarative_base(cls=Base)
Base.query = db_session.query_property()

class Config(Base):
    __tablename__ = 'config'
    name = Column(Text, nullable=False, unique=False)
    value = Column(Text, nullable=True, unique=False)
    active = Column(Boolean, default=True)

test_questioninfo_table = Table(
    'test_questioninfo', Base.metadata,
    Column(
        'test_id', Integer,
        ForeignKey('tests.id', onupdate='CASCADE', ondelete='CASCADE'),
        primary_key=True
    ),
    Column(
        'questioninfo_id', Integer,
        ForeignKey('question_info.id', onupdate='CASCADE', ondelete='CASCADE'),
        primary_key=True
    )
)

class Question(Base):
    __tablename__ = 'questions'
    name = Column(Text, nullable=False, unique=False)
    subject = Column(String(16), nullable=False, unique=False)
    question_image_url = Column(Text, default='', unique=False)
    answer_image_url = Column(Text, default='', unique=False)
    active = Column(Boolean, default=True)
    point = Column(Integer, default=2)
    type = Column(String(16), default='objective5', unique=False)
    answer = Column(String(32), default='', unique=False)
    question_info = relationship(
        'QuestionInfo',
        backref='question',
        uselist=False)

class QuestionInfo(Base):
    __tablename__ = 'question_info'
    correct_count = Column(Integer, default=0)
    incorrect_count = Column(Integer, default=0)
    error_rate = Column(Float, default=0.0)
    question_id = Column(Integer, ForeignKey('questions.id'))


class Test(Base):
    __tablename__ = 'tests'
    subject = Column(String(16), nullable=False, unique=False)
    time_duration = Column(Integer, default=1000*60*10) #10분
    time = Column(Date, default='')
    questioninfos = relationship(
        'QuestionInfo',
        backref=backref('tests', collection_class=set),
        secondary=test_questioninfo_table, collection_class=set
    )

    def getQuestions(self, time, subject):
        test = self.query.filter_by(time=time, subject=subject).first()
        print "item:", test.id, test.time_duration
        items = []
        total_point = 0
        for question_info in test.questioninfos:
            item = {}
            item['id'] = question_info.id
            item['question_image_url'] = question_info.question.question_image_url
            item['explain_image_url'] = question_info.question.answer_image_url
            item['error_rate'] = int(question_info.error_rate * 100)
            item['point'] = question_info.question.point
            total_point += question_info.question.point
            item['answer'] = question_info.question.answer
            item['type'] = question_info.question.type
            items.append(item)
        result = {"data":items, "total_point":total_point, "total_time":test.time_duration, "data_count":len(items)}
        return result


class UserAnswer(Base):
    __tablename__ = 'user_answer'
    user = Column(Text, nullable=False, unique=False)
    time = Column(DATETIME, default=datetime.datetime.now())
    date = Column(DATE, default=datetime.datetime.now().date())
    subject = Column(String(16), nullable=False, unique=False)
    answers = Column(Text, nullable=False, unique=False)

