#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'jaehwacho'
from flask import Flask, jsonify, request
from . import config
from .model import Config, Test, UserAnswer, QuestionInfo, init_engine, db_session
import pickle
import datetime
import json

application = Flask(__name__)
application.config.from_object(config)
init_engine(application.config['DB_URI'])

#Beta테스트시 날짜를 변경하면서 하게 하기 위해서 추가(현재 서버 날짜를 수정시도)
#default sms
def getTodayDateTime():
    #today = datetime.datetime(2013,11,14, 11,12,0)
    today = datetime.datetime.now()
    return today

#[API]1.시험문제가져오기
@application.route('/api/v1.0/exam/<index>', methods=['GET'])
def get_exam(index):
    subject = request.args.get('subject')
    if subject in ['korean', 'math', 'english']:
        if index == "today":
            time = getTodayDateTime().date()
        elif index == "yesterday":
            time = getTodayDateTime().date() - datetime.timedelta(1)
        else:
            error_code = 1102   #입력파라메터오류
            return jsonify(result={}, error=error_code)

        test = Test()
        result = test.getQuestions(time=time, subject=subject)
        error_code = 0
    else:
        error_code = 1102   #입력파라메터오류

    return jsonify(result=result, error=error_code)

#[API]2.시험답안지제출
@application.route('/api/v1.0/exam/<index>', methods=['POST'])
def send_answer(index):
    if index == "today":
        answers = request.form.get("answers")
        user = request.form.get("user")
        subject = request.form.get("subject")

        #오늘 이미 문제를 제출한 사용자 체크
        today = getTodayDateTime().date()
        userAnswer = UserAnswer.query.filter_by(user=user,
                                                subject=subject,
                                                date = today,
                                                ).first()
        if userAnswer!=None:
            today = getTodayDateTime().date()
            if today == userAnswer.time.date():
                result = {"result":{},"error":2001} # [Error][2001]오늘 문제를 이미 제출한 사용자
                return jsonify(result)

        #제출한 문서가 json타입인지 검사
        try:
            answers = json.loads(answers)
        except:
            result = {"result":{},"error":1102} #입력파라메터오류
            return jsonify(result)

        #사용자 정보 추가
        userAnswer = UserAnswer(user=user, 
                                subject=subject, 
                                answers=json.dumps(answers),
                                time=getTodayDateTime(),
                                date=getTodayDateTime().date())
        db_session.add(userAnswer)
        db_session.commit()
        result = {"result":{}, "error":0}
    else:
        result = {"result":{},"error":1102} #입력파라메터오류
    return jsonify(result)

#[API]3.앱 Config 정보가져오기
@application.route('/api/v1.0/config', methods=['GET'])
def get_config():
    result = {}
    data = Config.query.filter_by(active=True)
    for item in data:
        result[item.name] = pickle.loads(item.value)
    return jsonify(result=result)